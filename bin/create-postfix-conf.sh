#!/bin/bash

set -e

source .env

VOLUME_PATH=./volume
POSTFIX_PATH=$VOLUME_PATH/postfix
POSTFIX_CONF=$POSTFIX_PATH/main.cf

mkdir -p $POSTFIX_PATH
echo "" > $POSTFIX_CONF

if [ ! -z "$MTP_HOST" ]; then
  echo "myhostname = $MTP_HOST" >> $POSTFIX_CONF
fi

if [ ! -z "$MTP_DESTINATION" ]; then
  echo "mydestination = $MTP_DESTINATION" >> $POSTFIX_CONF
fi

if [ ! -z "$MTP_BANNER" ]; then
  echo "smtpd_banner = $MTP_BANNER" >> $POSTFIX_CONF
fi

if [ ! -z "$MTP_RELAY_DOMAINS" ]; then
  echo "relay_domains = $MTP_RELAY_DOMAINS" >> $POSTFIX_CONF
fi

cat <<EOT >> $POSTFIX_CONF
mydomain                  = $DOMAINNAME
mynetworks                = 127.0.0.1/32 192.168.0.0/16 172.16.0.0/12 10.0.0.0/8
header_checks             = regexp:/etc/postfix/header_checks

# use only IPv4
inet_interfaces           = all
inet_protocols            = ipv4

#
smtp_host_lookup          = native

# apply the bounce template
bounce_template_file      = /etc/postfix/bounce.cf

# set the list of error classes that are reported to the postmaster
notify_classes            = bounce, resource, software

# Consider a message as undeliverable, when delivery fails with a temporary
# error, and the time in the queue has reached the maximal_queue_lifetime limit.
# Specify 0 when mail delivery should be tried only once.
maximal_queue_lifetime    = $POSTFIX_MAXIMAL_QUEUE_LIFETIME
bounce_queue_lifetime     = $POSTFIX_MAXIMAL_QUEUE_LIFETIME

# configure TLS
smtpd_tls_cert_file       = /tmp/letsencrypt/etc/live/$SUBDOMAIN.$DOMAINNAME/fullchain.pem
smtpd_tls_key_file        = /tmp/letsencrypt/etc/live/$SUBDOMAIN.$DOMAINNAME/privkey.pem
smtpd_tls_security_level  = encrypt
smtpd_tls_ask_ccert = yes
smtpd_tls_auth_only = yes
smtp_use_tls = yes

# make Postfix aware of OpenDKIM/OpenDMARC and allow it to sign and verify mail
smtpd_milters             = inet:opendkim:8891, inet:opendmarc:8893
non_smtpd_milters         = \$smtpd_milters
milter_default_action     = accept

# Reduce the smtp_connect_timeout and smtp_helo_timeout values so that Postfix
# does not waste lots of time connecting to non-responding remote SMTP servers.
smtp_connect_timeout      = 5s
smtp_helo_timeout         = 5s

# Time to pause before accepting a new message, when the message arrival rate
# exceeds the message delivery rate
in_flow_delay             = 1s

# The maximal number of parallel deliveries to the same destination
default_destination_concurrency_limit = 20
relay_destination_concurrency_limit   = \$default_destination_concurrency_limit
EOT
