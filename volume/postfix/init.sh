#!/bin/sh

set -e

echo "/^Subject:/     WARN" >> /etc/postfix/header_checks

# create the sender user
adduser -D -H $SENDER_USER

# redirect from sender to the postmaster
echo "$SENDER_USER:   postmaster" >> /etc/postfix/aliases

# add root email
echo "root:   $POSTMASTER" >> /etc/postfix/aliases

# apply aliases updating
newaliases
