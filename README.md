# Postfix Send-Only SMTP Server

This repository contains files to run a Postfix server on a Docker using Docker Compose.

The Postfix server is configured as a Send-Only SMTP, so there are no IMAP/POP servers.

## Features

- Use Postfix v3.1.3 with TLS
- Use OpenDKIM Filter v2.10.3
- Use OpenDMARC Filter v1.3.2

## Setup

1. Create the SSL keys
2. Edit the `.env` file
3. Run the `setup.sh` script once to automatically:
    - create DKIM keys
    - generate Postfix config file
    - generate OpenDKIM config file
    - generate OpenDMARC config file
    - run containers
    - initialize the Postfix container
4. Publish the DKIM public keys
5. Generate and add a DMARC record
6. Configure the PTR records
7. Open the port 25 for Postfix
8. Run tests

To restart containers, use `./restart.sh`.

## Configure SSL with Let's encrypt

After installation, you can test your setup with [checktls.com](https://www.checktls.com/TestReceiver).

### Let's Encrypt Setup

To enable Let's Encrypt on your mail server, you have to make a directory to store your letsencrypt logs and configs.

```sh
mkdir -p ./volume/letsencrypt
cd ./volume/letsencrypt
```

Now get the certificate (modify `mail.myserver.tld`) and following the certbot
instructions.
This will need access to port 80 from the internet, adjust your firewall if
needed

```sh
docker run --rm -it \
  -v $PWD/log/:/var/log/letsencrypt/ \
  -v $PWD/etc/:/etc/letsencrypt/ \
  -p 80:80 \
  deliverous/certbot \
    certonly --standalone -d mail.myserver.tld
```

To renew your certificate just run (this will need access to port `443` from the internet, adjust your firewall if needed)

```
docker run --rm -it \
  -v $PWD/log/:/var/log/letsencrypt/ \
  -v $PWD/etc/:/etc/letsencrypt/ \
  -p 80:80 \
  -p 443:443 \
  deliverous/certbot renew
```

### Testing certificate

From your host:

```sh
docker exec postfix \
  openssl s_client -connect 0.0.0.0:25 -starttls smtp -CApath /etc/ssl/certs/
# or
docker exec postfix \
  openssl s_client -connect 0.0.0.0:143 -starttls imap -CApath /etc/ssl/certs/
```

And you should see the certificate chain, the server certificate and:

```sh
Verify return code: 0 (ok)
```

## Send testing email

Use http://www.appmaildev.com/en/dkim to get a testing email.

```sh
telnet mail.myserver.tld 25
HELO mail.myserver.tld
MAIL FROM: no-reply@mail.myserver.tld
RCPT TO: test-77a73151@appmaildev.com
DATA
subject: Test
Testing 1, 2, 3
.
quit
```

**Note** that if you're sending email from another container, you need to add it to the postfix's network. Also, you will need to add your container as a trusted host:

```sh
./bin/add-trusted-host.sh <container-name>
```

## Testing Tools

- SPF/DKIM/DMARC/DomainKey/RBL Test: http://www.appmaildev.com/en/dkim
- Brandon Checketts Email Validator: http://www.brandonchecketts.com/emailtest.php
- Send a signed email to: autorespond+dkim@dk.elandsys.com
- Send a signed email to: sa-test@sendmail.net
- Send a signed email to: check-auth@verifier.port25.com

## License

Under the GNU General Public License v3.0<br>
Copyright (C) 2019  Sébastien Demanou<br>
See [LICENSE](https://gitlab.com/demsking/postfix-send-only-smtp-server/blob/master/LICENSE) file for more details.
