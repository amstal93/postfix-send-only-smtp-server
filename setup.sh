#!/bin/sh

set -e

echo "Creating DKIM keys"
./bin/create-dkim-keys.sh

echo "Creating DKIM config file"
./bin/create-dkim-conf.sh

echo "Creating DMARC config file"
./bin/create-dmarc-conf.sh

echo "Creating Postfix config file"
./bin/create-postfix-conf.sh

echo "Starting containers..."
docker-compose up --build -d

echo "Configuring postfix container"
docker exec -it postfix /tmp/init.sh
